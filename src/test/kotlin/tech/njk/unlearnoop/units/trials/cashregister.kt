package tech.njk.unlearnoop.units.trials

data class Promotion(
  val buyCount: Int,
  val receiveCount: Int
) {
  companion object {
    fun from(str: String) =
      str.split("x").map {
        it.trim().toInt()
      }.let {
        Promotion(it[1], it[0])
      }
  }
}

data class CashRegister(
  val prices: Map<String, Double>,
  val promotions: Map<String, Promotion>
) {
  fun checkout(items: List<String>) =
    items.map {
      Pair(it, prices[it]!!)
    }.groupBy {
      it.first
    }.map {
      val tot = it.value.fold(0.0) { acc, pair ->
        acc + pair.second
      }
      Triple(it.key, it.value.size, tot)
    }
}
