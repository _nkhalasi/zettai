package tech.njk.unlearnoop.units.trials

import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import kotlin.random.Random

class AdditionTests {
  @Test
  fun `add two numbers`() {
    expectThat(6 + 5).isEqualTo(11)
    expectThat(100+23).isEqualTo(123)
  }

  private fun randomNatural() = Random.nextInt(from = 1, until = 100_000_000)

  @Test
  fun `zero identity`() {
    repeat(100) {
      val x = randomNatural()
      expectThat(x + 0).isEqualTo(x)
    }
  }

  @Test
  fun `computative property`() {
    repeat(100) {
      val x = randomNatural()
      val y = randomNatural()
      expectThat(x + y).equals(y + x)
    }

    @Test
    fun `associative property`() {
      repeat(100) {
        val x = randomNatural()
        val y = randomNatural()
        val z = randomNatural()
        expect {
          that(x + (y + z)).isEqualTo(x + y + z)
          that(y + (x + z)).isEqualTo(x + y + z)
          that(z + (x + y)).isEqualTo(x + y + z)
        }
      }
    }
  }
}
