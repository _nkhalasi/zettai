package tech.njk.unlearnoop.zettai.at

import org.junit.jupiter.api.Test
import org.opentest4j.AssertionFailedError
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEqualTo
import tech.njk.unlearnoop.zettai.at.utils.ApplicationForAT
import tech.njk.unlearnoop.zettai.at.utils.Step
import tech.njk.unlearnoop.zettai.at.utils.createList
import tech.njk.unlearnoop.zettai.at.utils.startTheApplication
import tech.njk.unlearnoop.zettai.domain.ListName
import tech.njk.unlearnoop.zettai.domain.ToDoItem
import tech.njk.unlearnoop.zettai.domain.ToDoList
import tech.njk.unlearnoop.zettai.domain.User
import tech.njk.unlearnoop.zettai.server.ToDoListHub
import tech.njk.unlearnoop.zettai.server.ZettaiHub

interface ScenarioActor {
  val name: String
}

class ToDoListOwner(override val name: String): ScenarioActor {
  fun canSeeTheirLists(listName: String, items: List<String>): Step = {
    val expectedList = createList(listName, items)
    val list = getToDoList(name, listName)
    expectThat(list).isEqualTo(expectedList)
  }

  fun cannotSeeTheList(listName: String): Step = {
    expectThrows<AssertionFailedError> {
      getToDoList(name, listName)
    }
  }

  fun asUser(): User = User(name)
}


class SeeAToDoList {
  private val frank = ToDoListOwner("frank")
  private val shoppingItems = listOf("carrots", "apples", "milk")
  private val frankList = createList("shopping", shoppingItems)

  private val bob = ToDoListOwner("bob")
  private val gardenItems = listOf("fix the fence", "mowing the lawn")
  private val bobList = createList("gardening", gardenItems)

  private val lists = listOf(
    frank.asUser() to listOf(frankList),
    bob.asUser() to listOf(bobList)
  ).toMap()

  @Test
  fun `List owner can see their lists`() {
    startTheApplication(lists).runScenario(
      frank.canSeeTheirLists("shopping", shoppingItems),
      bob.canSeeTheirLists("gardening", gardenItems)
    )
  }

  @Test
  fun `Only owners can see their lists`() {
    startTheApplication(lists).runScenario(
      frank.cannotSeeTheList("gardening"),
      bob.cannotSeeTheList("shopping")
    )
  }

  @Test
  fun `get list by user and name`() {
    val hub = ToDoListHub(lists)
    val myList = hub.getList(bob.asUser(), ListName("gardening"))
    expectThat(myList).isEqualTo(bobList)
  }

}


