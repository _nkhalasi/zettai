package tech.njk.unlearnoop.zettai.at.utils

import org.http4k.client.JettyClient
import org.http4k.core.*
import org.http4k.filter.ClientFilters
import org.http4k.server.Jetty
import org.http4k.server.asServer
import org.junit.jupiter.api.fail
import tech.njk.unlearnoop.zettai.domain.ListName
import tech.njk.unlearnoop.zettai.domain.ToDoItem
import tech.njk.unlearnoop.zettai.domain.ToDoList
import tech.njk.unlearnoop.zettai.domain.User
import tech.njk.unlearnoop.zettai.server.ToDoListHub
import tech.njk.unlearnoop.zettai.server.Zettai

class ApplicationForAT(val client: HttpHandler, private val server: AutoCloseable): Actions {
  fun runScenario(vararg steps: Step) {
    server.use {
      steps.onEach {
        it(this)
      }
    }
  }

  override fun getToDoList(user: String, listName: String): ToDoList {
    val response = client(Request(Method.GET, "/todo/$user/$listName"))
    return if (response.status == Status.OK)
      parseMyResponse(response.bodyString())
    else
      fail(response.toMessage())
  }

  private fun parseMyResponse(html: String): ToDoList {
    val nameRegex = "<h2>.*<".toRegex()
    val listName = ListName(extractListName(nameRegex, html))
    val itemsRegex = "<td>.*?<".toRegex()
    val items = itemsRegex.findAll(html)
      .map { ToDoItem(extractItemDesc(it)) }.toList()
    return ToDoList(listName, items)
  }

  private fun extractListName(nameRegex: Regex, html: String): String =
    nameRegex.find(html)?.value
      ?.substringAfter("<h2>")
      ?.dropLast(1)
      .orEmpty()

  private fun extractItemDesc(matchResult: MatchResult): String =
    matchResult.value.substringAfter("<td>").dropLast(1)
}

fun startTheApplication(lists: Map<User, List<ToDoList>>): ApplicationForAT {
  val port = 8081
  val server = Zettai(ToDoListHub(lists)).asServer(Jetty(8081))
  server.start()

  val client = ClientFilters
    .SetBaseUriFrom(Uri.of("http://localhost:$port/"))
    .then(JettyClient())

  return ApplicationForAT(client, server)
}

fun createList(listName: String, items: List<String>) = ToDoList(ListName(listName), items.map(::ToDoItem))

interface Actions {
  fun getToDoList(user: String, listName: String): ToDoList?
}

typealias Step = Actions.() -> Unit
