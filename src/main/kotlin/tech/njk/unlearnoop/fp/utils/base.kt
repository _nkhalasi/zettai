package tech.njk.unlearnoop.fp.utils

typealias FUN<A, B> = (A) -> B

infix fun <A, B, C> FUN<A, B>.andThen(other: FUN<B, C>): FUN<A, C> =
  { a: A -> other(this(a)) }

infix fun <A, B, C> FUN<A, B?>.andThenNN(other: FUN<B, C?>): FUN<A, C?> =
  { a: A -> this(a)?.let {  other(it) } }

operator fun <A, B, C> FUN<A, B>.plus(other: FUN<B, C>): FUN<A, C> =
  this.andThen(other)
