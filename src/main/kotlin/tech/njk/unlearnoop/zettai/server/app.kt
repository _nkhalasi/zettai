package tech.njk.unlearnoop.zettai.server

import org.http4k.core.*
import org.http4k.routing.bind
import org.http4k.routing.path
import org.http4k.routing.routes
import org.http4k.server.Jetty
import org.http4k.server.asServer
import tech.njk.unlearnoop.fp.utils.andThen
import tech.njk.unlearnoop.fp.utils.plus
import tech.njk.unlearnoop.zettai.domain.*

data class Zettai(val hub: ZettaiHub) : HttpHandler {
  val routes = routes(
    "/todo/{user}/{list}" bind Method.GET to ::getToDoList
  )
  override fun invoke(req: Request): Response = routes(req)

  private fun extractList(req: Request): Pair<User, ListName> {
    val user = req.path("user").orEmpty()
    val list = req.path("list").orEmpty()
    return User(user) to ListName(list)
  }

  private fun fetchListContent(listId: Pair<User, ListName>): ToDoList =
    hub.getList(listId.first, listId.second)
      ?: error("list unknown")

  private fun renderHtml(todoList: ToDoList): HtmlPage =
    HtmlPage("""
       <html>
        <body>
         <h1>Zettai</h1>
         <h2>${todoList.listName.name}</h2>
         <table>
          <tbody>${renderItems(todoList.items)}</tbody>
         </table>
        </body>
       </html>
    """.trimIndent() )

  private fun renderItems(items: List<ToDoItem>) =
    items.map {
      """<tr><td>${it.description}</td></tr>""".trimIndent()
    }.joinToString("")

  private fun createResponse(html: HtmlPage): Response =
    Response(Status.OK).body(html.raw)

  private val getToDoListFn: (Request) -> Response =
    ::extractList + ::fetchListContent + ::renderHtml + ::createResponse

  private fun getToDoList(req: Request): Response = getToDoListFn(req)
}

interface ZettaiHub {
  fun getList(user: User, listName: ListName): ToDoList?
}

class ToDoListHub(val lists: Map<User, List<ToDoList>>): ZettaiHub {
  override fun getList(user: User, listName: ListName): ToDoList? =
      lists[user]
      ?.firstOrNull { it.listName == listName }

}

fun main() {
  val items = listOf("write chapter", "insert code", "draw diagrams")
  val todoList = ToDoList(ListName("book"), items.map(::ToDoItem))
  val lists = mapOf(User("uberto") to listOf(todoList))
  val app = Zettai(ToDoListHub(lists))
  app.asServer(Jetty(8080)).start()
  println("Server started at http://localhost:8080/todo/uberto/book")
}
