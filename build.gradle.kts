import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.5.31"
}

group = "tech.njk.unlearnoop"
version = "1.0-SNAPSHOT"

repositories {
  mavenCentral()
}

val junitVersion: String by project
val junitLauncherVersion: String by project
val striktVersion: String by project
val http4kVersion: String by project

dependencies {
  api("org.http4k", "http4k-core", http4kVersion)
  api("org.http4k", "http4k-server-jetty", http4kVersion)
  testImplementation("org.http4k", "http4k-client-jetty", http4kVersion)
  testImplementation(kotlin("test"))
  testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
  testRuntimeOnly("org.junit.platform", "junit-platform-launcher", junitLauncherVersion)
  testApi("io.strikt", "strikt-core", striktVersion)
}

tasks.test {
  useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "1.8"
}
